package com.jblearning.mortgagev3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

public class Mortgage {
  public final DecimalFormat MONEY
               = new DecimalFormat( "$#,##0.00" );
  public static final String PREFERENCE_AMOUNT = "amount";
  public static final String PREFERENCE_YEARS = "years";
  public static final String PREFERENCE_RATE = "rate";

  public String filename = "preferences.txt";

  private float amount;
  private int years;
  private float rate;

  public Mortgage( Context context) {
//    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
//    setAmount(pref.getFloat(PREFERENCE_AMOUNT, 100000.0f ));
//    setYears(pref.getInt(PREFERENCE_YEARS, 30 ));
//    setRate(pref.getFloat(PREFERENCE_RATE, 0.035f ));

    File prefsFile = new File(context.getFilesDir(), filename);

      try {
          BufferedReader bufferedReader = new BufferedReader (new  InputStreamReader (new FileInputStream(filename));

          String inputLine = bufferedReader.readLine();
          String[] data = inputLine.split(",");
          setAmount(Float.parseFloat(data[0]));
          setYears(Integer.parseInt(data[1]));
          setRate(Float.parseFloat(data[2]));

    } catch (Exception e) {
        e.printStackTrace();
    }
  }

  public void setPreferences(Context context) {
//    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
//    SharedPreferences.Editor editor = pref.edit();
//    editor.putFloat(PREFERENCE_AMOUNT, amount);
//    editor.putInt(PREFERENCE_YEARS, years);
//    editor.putFloat(PREFERENCE_RATE, years);
//    editor.commit();

    File prefsFile = new File(context.getFilesDir(), filename);
    String fileContents;
    String[] data = {Float.toString(amount), Integer.toString(years), Float.toString(rate)};
    fileContents = TextUtils.join(",", data);
    FileOutputStream outputStream;

    try {
        outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
        outputStream.write(fileContents.getBytes());
        outputStream.close();
    } catch (Exception e) {
        e.printStackTrace();
    }
  }

  public void setAmount( float newAmount ) {
    if( newAmount >= 0 )
      amount = newAmount;
  }

  public void setYears( int newYears ) {
    if( newYears >= 0 )
      years = newYears;
  }

  public void setRate( float newRate ) {
    if( newRate >= 0 )
      rate = newRate;
  }

  public float getAmount( ) {
    return amount;
  }

  public String getFormattedAmount( ) {
    return MONEY.format( amount );
  }

  public int getYears( ) {
    return years;
  }

  public float getRate( ) {
    return rate;
  }

  public float monthlyPayment( ) {
    float mRate = rate / 12;  // monthly interest rate
    double temp = Math.pow( 1/( 1 + mRate ), years * 12 );
    return amount * mRate / ( float ) ( 1 - temp );
  }

  public String formattedMonthlyPayment( ) {
    return MONEY.format( monthlyPayment( ) );
  }

  public float totalPayment( ) {
    return monthlyPayment( ) * years * 12;
  }

  public String formattedTotalPayment( ) {
    return MONEY.format( totalPayment( ) );
  }
}